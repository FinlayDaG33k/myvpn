FROM arm32v7/debian:stretch

LABEL maintainer=FinlayDaG33k

# Select the frontend for debconf
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Add some dependencies
RUN apt-get update && apt-get install -y \
    apt-utils \
    procps \
    ca-certificates \ 
    openvpn \
    git \
    dhcpcd5 \
    tar \
    wget \
    grep \
    iptables-persistent \
    dnsutils \
    expect \
    whiptail \
    net-tools

# Add the PiVPN installer
ADD https://install.pivpn.io install.sh

# Add a user
RUN useradd -m pivpn

# Add rule(s) to the iptables
RUN iptables -t nat -A POSTROUTING -s 10.8.0.0/8 -o eth0 -j MASQUERADE

# Expose the PiVPN UDP port
EXPOSE 1194/udp

# Make OpenVPN start when the container starts (unless another command was specified)
CMD ["service", "openvpn", "start"]
#!/bin/bash
# Install docker if needed
docker_loc=$(command -v docker)
if [ -x "${docker_loc}" ]; then
  # Everything gud
  echo "[INFO] Docker found at \"${docker_loc}\""
else
  # Install Docker and add us to the docker group
  echo "[WARN] Docker not found, Installing it for you"
  curl -fsSL get.docker.com | sh
  sudo usermod -aG docker $(whoami)
  newgrp docker
fi

# Build container
docker build -t myvpn/openvpn:latest -f https://gitlab.com/FinlayDaG33k/myvpn/raw/master/dockerfiles/openvpn.dockerfile .

# Make a directory to store all the data
datadir="/home/$(whoami)/pivpn_data"
mkdir -p $datadir

# Build the command for starting the container
command="docker run -i -d -p 1194:1194/udp --cap-add=NET_ADMIN --device=/dev/net/tun --restart=unless-stopped --volume=$datadir/pivpn:/opt/pivpn --volume=$datadir/openvpn:/etc/openvpn --volume=$datadir/profiles:/home/pivpn/ovpns myvpn/openvpn:latest bash"

# Start the container and install PiVPN
container="$($command)"
docker exec -it $container bash install.sh

# Restart the container
echo "::: Restarting container"
docker restart $container
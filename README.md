# MyVPN

A simple to use OpenVPN setup using Docker and PiVPN.  

Thanks to the PiVPN project as well as the InnovativeInventor whose code I've adapted for this project.

# Installation
Installation is easy, just run the following command, follow some simple prompts and it should be all good!
> curl -L https://gitlab.com/FinlayDaG33k/myvpn/raw/master/install.sh | bash